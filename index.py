import re
from datetime import datetime

# Creative Technology Project - Mr Moath
# - Marco Escaleira
# - Ricardo Soares

VALID_ANSWERS = [1,2,3,4,5,6]

defaultError = "\nSorry, something went wrong."
defaultIdErrorMessage = "\nSorry, that employee ID doesn't exist."

idEmployee = [0]
firstNameEmployee = ["Steve"]
surnameEmployee = ["Smith"]
startDateEmployee = ["15-10-2018"]
positionEmployee = ["Quality Engineer"]
departmentEmployee = ["Operations"]
basicSalaryEmployee = ["2500"]
contactNumberEmployee = ["7878456783"]
emailAddressEmployee = ["steve.smith@meta.com"]
isEmployeeActive = [True]

idCounter = 0

# Function to print employee data in a organized way
def printEmployeeData(i):
  print("-----------------------------------------------")
  print("ID: " + str(idEmployee[i]) )
  print("Name: " + firstNameEmployee[i] + " " + surnameEmployee[i])
  print("Position: " + positionEmployee[i] + " | Department: " + departmentEmployee[i])
  print("Start date: " + startDateEmployee[i] + " | Salary: " + basicSalaryEmployee[i])
  print("Contacts: " + contactNumberEmployee[i] + " | " + emailAddressEmployee[i])
  print("-----------------------------------------------")

# Function to validate if string has numbers
def doesStringHasNumbers(value):
    return any(i.isdigit() for i in value)

# Function to validate text
def textValidation(inputText):
  while True:
    try:
      value = input(inputText)
    except ValueError:
      print(defaultError)
      continue

    if len(value) < 2:
      print("- Sorry, please introduce at least 2 characters. \n")
      continue
    elif doesStringHasNumbers(value):
      print("- Sorry, value should not have any number. \n")
    else:
      break

  return value

def dateValidation(inputText):
  while True:
    try:
      value = input(inputText)

      value = datetime.strptime(value, "%d-%m-%Y")
    except ValueError:
      print("- Sorry, please introduce a valid date. \n")
      continue

    break

  return value.strftime("%d-%m-%Y")

# For numbers validations
def numberValidation(inputText):
  while True:
    try:
      value = int(input(inputText))
    except ValueError:
      print("- Sorry, please introduce only numbers. \n")
      continue

    break

  return str(value)

# For numbers validations
def emailValidation(inputText):
  while True:
    try:
      value = input(inputText)
    except ValueError:
      print(defaultError)
      continue
    
    if not re.match(r"[^@]+@[^@]+\.[^@]+", value):
      print("- Sorry, please introduce a correct email. \n")
      continue

    break

  return value


# Function with logic to add an employee
def addEmployee():
  print("\n\nCreating an employee\n")

  # Collect employee data
  firstName = textValidation("First name: ")
  surname = textValidation("Surname: ")
  startDate = dateValidation("Start date (e.g. DD-MM-YYYY ): ")
  position = textValidation("Position: ")
  department = textValidation("Department: ")
  basicSalary = numberValidation("Basic salary: ")
  contactNumber = numberValidation("Contact number: ")
  emailAddress = emailValidation("Email address: ")

  # Add introduced data into arrays 
  idEmployee.append(idCounter + 1)
  firstNameEmployee.append(firstName)
  surnameEmployee.append(surname)
  startDateEmployee.append(startDate)
  positionEmployee.append(position)
  departmentEmployee.append(department)
  basicSalaryEmployee.append(basicSalary)
  contactNumberEmployee.append(contactNumber)
  emailAddressEmployee.append(emailAddress)
  isEmployeeActive.append(True)

  print("\n\n" + firstName + " has been added successfully!")
  menu(False)

# Function with logic to modify an employee
def modifyEmployee():
  print("\n\nModify an employee")
  print("- Press 'b' to go back\n")

  id = input("ID to edit: ")

  if (id == 'b'):
    return
  
  try:
    id = int(id)
  except ValueError:
    print("\nPlease insert only numbers.")
    modifyEmployee()

  # Check if ID is within range of existing users and that user is active
  if (id >= 0 and id <= len(idEmployee) and isEmployeeActive[id]):
    print("Which field you want to edit?")
    print("\n1. First Name ( " + firstNameEmployee[id] + " )")
    print("2. Surname ( " + surnameEmployee[id] + " )")
    print("3. Start date ( " + startDateEmployee[id] + " )")
    print("4. Position ( " + positionEmployee[id] + " )")
    print("5. Department ( " + departmentEmployee[id] + " )")
    print("6. Basic Salary ( " + basicSalaryEmployee[id] + " )")
    print("7. Contact Number ( " + contactNumberEmployee[id] + " )")
    print("8. Email address ( " + emailAddressEmployee[id] + " )")
    
    fieldToEdit = int(input("\nAnswer: "))

    if fieldToEdit == 1:
      newValue = textValidation("New First Name: ")
      firstNameEmployee[id] = newValue
    elif fieldToEdit == 2:
      newValue = textValidation("New Surname: ")
      surnameEmployee[id] = newValue
    elif fieldToEdit == 3:
      newValue = dateValidation("New Start date: ")
      startDateEmployee[id] = newValue
    elif fieldToEdit == 4:
      newValue = textValidation("New Position: ")
      positionEmployee[id] = newValue
    elif fieldToEdit == 5:
      newValue = textValidation("New Department: ")
      departmentEmployee[id] = newValue
    elif fieldToEdit == 6:
      newValue = numberValidation("New Basic Salary: ")
      basicSalaryEmployee[id] = newValue
    elif fieldToEdit == 7:
      newValue = numberValidation("New Contact Number: ")
      contactNumberEmployee[id] = newValue
    elif fieldToEdit == 8:
      newValue = emailValidation("New Email address: ")
      emailAddressEmployee[id] = newValue
  
    print("\nField has been edited successfully!")
    menu(False)
  else:
    print("\nID doesn't exist, please try again.")
    modifyEmployee()

# Function to list all employees
def listEmployees():
  print("\n\nList all employees")

  i = 0
  while (i < len(idEmployee)):
    # Check if employee is active before printing
    if (isEmployeeActive[i] == True):
      printEmployeeData(i)
    i += 1
  
  menu(False)

# Function with logic to search for an employee
def searchEmployee():
  print("\n\nSearch for an employee")
  print("- Press 'b' to go back\n")
  
  id = input("ID to search: ")

  if (id == 'b'):
    return

  try:
    id = int(id)
  except ValueError:
    print("\nPlease insert only numbers.")
    searchEmployee()

  if (id >= 0 and id < len(idEmployee) and isEmployeeActive[id]): 
    printEmployeeData(id)
    menu(False)
  else: 
    print(defaultIdErrorMessage)
    searchEmployee()

# Function with logic to delete an employee
def deleteEmployee():
  try:
    print("\n\nDelete an employee")
    print("- Press 'b' to go back\n")

    id = input("ID to delete: ")

    if (id == 'b'):
      return
    
    id = int(id)

    # Check if employee ID exists before starting to remove
    if (id >= 0 and id < len(idEmployee) and isEmployeeActive[id]):
      isEmployeeActive[id] = False

      print("\n" + firstNameEmployee[id] + " has been deleted successfully!")
      menu(False)
    else:
      print(defaultIdErrorMessage)
      deleteEmployee()

  except ValueError:
    print("\nPlease insert only numbers.")
    deleteEmployee()

# Function to handle the user input on the menu
def handleAnswer(option):
  try:
    # Convert option to an integer variable
    response = int(option)

    # Validate if response is within the possible options
    if response not in VALID_ANSWERS:
      raise ValueError('Invalid option choosen')
    
    if response == 1:
      addEmployee()
    elif response == 2:
      modifyEmployee()
    elif response == 3:
      listEmployees()
    elif response == 4:
      searchEmployee()
    elif response == 5:
      deleteEmployee()
    elif response == 6:
      exit()

    menu(False)

  except ValueError:
    print("\n\nPlease insert a correct menu answer.")
    menu(False)


def menu (showBeggining):
  if showBeggining:
    print("\nWelcome to CompMan !")
    print("Version 1.0 - Employee Record System")

  # Present the menu to the user
  print("\n\nUser menu")
  print("\n1. Add an employee")
  print("2. Modify an employee")
  print("3. List all employees")
  print("4. Search for an employee")
  print("5. Delete an employee")
  print("6. Exit the program")

  # Allow user to choose an option from the menu
  answer = input("\nAnswer: ")

  # Call handleAnswer function to handle the input from user and do correct action
  handleAnswer(answer)

# Start the program for the first time
menu(True)